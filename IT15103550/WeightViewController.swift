//
//  WeightViewController.swift
//  IT15103550
//
//  Created by Yoosuf, Aadil on 8/16/18.
//  Copyright © 2018 Yoosuf, Aadil. All rights reserved.
//

import UIKit

class WeightViewController: UIViewController {

    @IBOutlet weak var txbGram: UITextField!
    @IBOutlet weak var txbKilogram: UITextField!
    @IBOutlet weak var txbPound: UITextField!
    @IBOutlet weak var txbOunce: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func txbChanged(_ sender: Any) {
        
        let text = ( sender as! UITextField).text
        
        if text == nil || text!.isEmpty {
            txbOunce.text = ""
            txbPound.text = ""
            txbKilogram.text = ""
            txbGram.text = ""
            return
        }
        
        let value = Double.init(text!)
        
        if value == nil {
            return
        }
        
        if (( sender as! UITextField).isEditing ) {
            
            if ( sender as! UITextField) == self.txbGram {
                
                setKiloValue( value: value! / 1000 )
                setPoundValue( value: value! / 453.59237 )
                setOunceValue( value: value! / 28.34952 )
                
            } else if ( sender as! UITextField ) == self.txbKilogram {
                
                setGramValue( value: value! * 1000 )
                setPoundValue( value: value! / 0.45359237 )
                setOunceValue( value: value! / 0.02834952 )
                
            } else if ( sender as! UITextField ) == self.txbPound {
                
                setGramValue( value: value! * 453.59237 )
                setKiloValue( value: value! * 0.45359237 )
                setOunceValue( value: value! * 16 )
                
            } else if ( sender as! UITextField ) == self.txbOunce {
                
                setGramValue( value: value! * 28.34952 )
                setKiloValue( value: value! * 0.02834952 )
                setPoundValue( value: value! / 16 )
            }
        }
    }
    
    func setGramValue(value: Double) {
        
        txbGram.text = "\(value)"
    }
    
    func setKiloValue(value: Double) {
        
        txbKilogram.text = "\(value)"
    }
    
    func setPoundValue(value:Double) {
        
        txbPound.text = "\(value)"
    }
    
    func setOunceValue(value:Double) {
        
        txbOunce.text = "\(value)"
    }
}
