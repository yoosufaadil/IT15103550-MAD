//
//  DistanceViewController.swift
//  IT15103550
//
//  Created by Yoosuf, Aadil on 8/16/18.
//  Copyright © 2018 Yoosuf, Aadil. All rights reserved.
//

import UIKit

class DistanceViewController: UIViewController {

    @IBOutlet weak var txbMeter: UITextField!
    @IBOutlet weak var txbFoot: UITextField!
    @IBOutlet weak var txbYArd: UITextField!
    @IBOutlet weak var txbKm: UITextField!
    @IBOutlet weak var txbMile: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func txbChanged(_ sender: Any) {
        let text = (sender as! UITextField).text
        
        if text == nil || text!.isEmpty {
            txbMeter.text = ""
            txbFoot.text = ""
            txbYArd.text = ""
            txbKm.text = ""
            txbMile.text = ""
            return
        }
        
        let value = Double.init(text!)
        if value == nil {
            return
        }
        
        if ((sender as! UITextField).isEditing) {
            if (sender as! UITextField) == self.txbMeter {
                setFootValue(value: value! / 0.3048)
                setYardValue(value: value! * 1.0936)
                setKmValue(value: value! / 1000)
                setMileValue(value: value! * 0.00062137)
            } else if (sender as! UITextField) == self.txbFoot {
                setMeterValue(value: value! * 0.3048)
                setYardValue(value: value! * 0.33333)
                setKmValue(value: value! / 3280.8)
                setMileValue(value: value! * 0.00018939)
            } else if (sender as! UITextField) == self.txbYArd {
                setMeterValue(value: value! / 1.0936)
                setFootValue(value: value! / 0.33333)
                setKmValue(value: value! )
            } else if (sender as! UITextField) == self.txbKm {
                
            } else if (sender as! UITextField) == self.txbMile {
                
            }
        }
    }
        
        func setMeterValue(value: Double) {
            txbMeter.text = "\(value)"
        }
        
        func setFootValue(value: Double) {
            txbFoot.text = "\(value)"
        }
        
        func setYardValue(value: Double) {
            txbYArd.text = "\(value)"
        }
        
        func setKmValue(value: Double) {
            txbKm.text = "\(value)"
        }
        
        func setMileValue(value: Double) {
            txbMile.text = "\(value)"
        }
    
}
